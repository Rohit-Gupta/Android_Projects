package com.example.rohit.location2_2;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Resources;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.ActivityRecognition;
import com.google.android.gms.location.DetectedActivity;
import com.google.android.gms.location.LocationServices;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks,GoogleApiClient.OnConnectionFailedListener{

    private TextView textView;
    private Button button1;
    private Button button2;
    protected GoogleApiClient googleApiClient;
    protected BroadcastReceiver broadcastReceiver;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        textView=(TextView) findViewById(R.id.detectedActivities);
        button1=(Button)findViewById(R.id.request_activity_updates_button);
        button2=(Button)findViewById(R.id.remove_activity_updates_button);
        buildGoogleApiClient();
    }

    private void buildGoogleApiClient() {
        googleApiClient=new GoogleApiClient.Builder(this)
                .addApi(LocationServices.API)
                .addOnConnectionFailedListener(this)
                .addConnectionCallbacks(this)
                .build();
    }

    @Override
    public void onConnected(Bundle bundle) {

    }

    @Override
    protected void onStart() {
        super.onStart();
        googleApiClient.connect();
    }

    @Override
    protected void onStop() {
        super.onStop();
        googleApiClient.disconnect();
    }

    @Override
    protected void onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(broadcastReceiver);
        super.onPause();
    }

    @Override
    protected void onResume() {
        LocalBroadcastManager.getInstance(this).registerReceiver(broadcastReceiver,new IntentFilter("com.example.rohit.location2_2.BROADCAST_ACTION"));
        super.onResume();
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }

    public void requestActivityUpdatesButtonHandler(View view){
        if(!googleApiClient.isConnected()){
            Toast.makeText(this,getString(R.string.not_connected),Toast.LENGTH_LONG).show();
            return;
        }
        ActivityRecognition.ActivityRecognitionApi.removeActivityUpdates(googleApiClient,)
    }

    class ActivityDetectionBroadcastReciever extends BroadcastReceiver{

        protected static final String TAG="reciever";

        public String getActivityString(int detectedActivityType){
            Resources resources=this.getResources();
            switch (detectedActivityType){
                case DetectedActivity.IN_VEHICLE:return resources.getString(R.string.id_vehicle);
                case DetectedActivity.ON_BICYCLE:return resources.getString(R.string.on_bycycle);
                case DetectedActivity.ON_FOOT:return resources.getString(R.string.on_foot);
                case DetectedActivity.RUNNING:return resources.getString(R.string.running);
                case DetectedActivity.STILL:return resources.getString(R.string.still);
                case DetectedActivity.TILTING:return resources.getString(R.string.tilting);
                case DetectedActivity.UNKNOWN:return resources.getString(R.string.unknown);
                case DetectedActivity.WALKING:return resources.getString(R.string.walking);
                default:return resources.getString(R.string.unidentifiable_activity);
            }
        }
        @Override
        public void onReceive(Context context, Intent intent) {
            ArrayList<DetectedActivity> updatedActivities=intent.getParcelableArrayListExtra("com.example.rohit.location2_2.ACTIVITY_EXTRA");

            String strStatus="";
            for(DetectedActivity thisActivity: updatedActivities){
                strStatus+=getActivityString(thisActivity.getType())+ thisActivity.getConfidence()+"%\n";
            }
            textView.setText(strStatus);
        }
    }
}
