package com.example.rohit.location2_2;

import android.app.IntentService;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;

import com.google.android.gms.location.ActivityRecognitionResult;
import com.google.android.gms.location.DetectedActivity;

import java.util.ArrayList;

/**
 * Created by rohit on 9/1/18.
 */

public class DetectedActivitiesIntentService extends IntentService{
    protected static final String TAG="detection_is";
    public DetectedActivitiesIntentService(){
        super(TAG);
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        ActivityRecognitionResult result=ActivityRecognitionResult.extractResult(intent);
        Intent localIntent=new Intent("com.example.rohit.location2_2"+".BROADCAST_ACTION");
        ArrayList<DetectedActivity> detectedActivities=(ArrayList) result.getProbableActivities();
        localIntent.putExtra("com.example.rohit.location2_2.ACTIVITY_EXTRA",detectedActivities);
        LocalBroadcastManager.getInstance(this).sendBroadcast(localIntent);
    }
}
