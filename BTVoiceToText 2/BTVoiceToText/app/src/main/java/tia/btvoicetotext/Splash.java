package tia.btvoicetotext;

/**
 * Created by DhruvPathak on 05/01/18.
 */

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.content.Intent;
import android.os.Bundle;
import android.os.Process;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

public class Splash extends AppCompatActivity {
    final Splash SplashScreen = this;
    private Thread SplashThread;
    BluetoothAdapter bluetoothAdapter;
    private int count =0;

    class SThread extends Thread {
        SThread() {
        }

        public void run() {
            while (!Splash.this.bluetoothAdapter.isEnabled()) {
                count++;
                try {
                    synchronized (this) {
                        wait(5000);
                    }
                } catch (InterruptedException e) {
                } finally {
                    Splash.this.finish();
                    Intent i = new Intent();
                    i.setClass(Splash.this.SplashScreen, VoiceCommand.class);
                    Splash.this.startActivity(i);
                }
            }
            if(count==0)
            {
                Splash.this.finish();
                Intent i = new Intent();
                i.setClass(Splash.this.SplashScreen, VoiceCommand.class);
                Splash.this.startActivity(i);
            }
        }
    }

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash);
        this.bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (this.bluetoothAdapter == null) {
            Toast.makeText(this, "Bluetooth is not available", Toast.LENGTH_SHORT).show();
            finish();
        }
        if (!this.bluetoothAdapter.isEnabled()) {
            Toast.makeText(this, "ENABLING BLUETOOTH !", Toast.LENGTH_SHORT).show();
            this.bluetoothAdapter.enable();
        }
        this.SplashThread = new SThread();
        this.SplashThread.start();
    }

    public void onBackPressed() {
        this.bluetoothAdapter.disable();
        Toast.makeText(this, "DISABLING BLUETOOTH !", Toast.LENGTH_SHORT).show();
        Process.killProcess(Process.myPid());
        finish();
    }


}
