package com.example.rohit.otp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.security.SecureRandom;

public class MainActivity extends AppCompatActivity
{
    TextView tv;
    Button bt;
    int otp=0;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        tv=(TextView)findViewById(R.id.otp_show);
        bt=(Button)findViewById(R.id.otp);

        bt.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                otp=generateRandomNumber();
                tv.setText(String.valueOf(otp));
            }
        });
    }

    public int generateRandomNumber()
    {
        int randomNumber;
        int range = 9;  // to generate a single number with this range, by default its 0..9
        int length = 6; // by default length is 4

        SecureRandom secureRandom = new SecureRandom();
        String s = "";
        for (int i = 0; i < length; i++)
        {
            int number = secureRandom.nextInt(range);
            if (number == 0 && i == 0)
            { // to prevent the Zero to be the first number as then it will reduce the length of generated pin to three or even more if the second or third number came as zeros
                i = -1;
                continue;
            }
            s = s + number;
        }

        randomNumber = Integer.parseInt(s);

        return randomNumber;
    }
}