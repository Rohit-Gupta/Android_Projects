package com.example.root.amuser;

import android.content.Context;
import com.readystatesoftware.sqliteasset.SQLiteAssetHelper;

/**
 * Created by root on 8/10/17.
 */

public class DatabaseOpenHelper extends SQLiteAssetHelper {

    private static final String DATABASE_VERSION="sorted.db";
    private static final int DATABASE_NAME=1;

    public DatabaseOpenHelper(Context context){
        super(context, DATABASE_VERSION,null, DATABASE_NAME);
    }
}
