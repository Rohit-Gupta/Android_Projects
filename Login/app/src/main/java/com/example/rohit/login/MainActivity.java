package com.example.rohit.login;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class MainActivity extends AppCompatActivity
{
    EditText name, email, password;
    Button login, register;
    private static final String REGISTER_URL="http://198.162.0.100/register.php";

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        int internet = ContextCompat.checkSelfPermission(this, Manifest.permission.INTERNET);
        if (internet != PackageManager.PERMISSION_GRANTED)
        {
            Toast.makeText(MainActivity.this,"Internet Access is not allowed",Toast.LENGTH_LONG).show();
        }
        else
        {
            Toast.makeText(MainActivity.this,"Internet Access is allowed",Toast.LENGTH_LONG).show();
        }

        name=(EditText)findViewById(R.id.name);
        email=(EditText)findViewById(R.id.email);
        password=(EditText)findViewById(R.id.password);
        login=(Button)findViewById(R.id.login);
        register=(Button)findViewById(R.id.register);

        register.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                String nm=name.getText().toString().trim().toLowerCase();
                String emid=email.getText().toString().trim().toLowerCase();
                String pass=password.getText().toString().trim().toLowerCase();
                Register(nm,emid,pass);
            }
        });
    }

    private void Register(String nm, String emid, String pass)
    {
        String urlsuffix="?username"+nm+"?password"+pass+"?email"+emid;
        class Registration extends AsyncTask<String,Void,String>
        {
            ProgressDialog progressDialog;

            @Override
            protected void onPreExecute()
            {
                super.onPreExecute();
                progressDialog=ProgressDialog.show(MainActivity.this,"Please Wait",null,true,true);
            }

            @Override
            protected String doInBackground(String... strings)
            {
                String s=strings[0];
                BufferedReader bufferedReader=null;
                try
                {
                    URL url=new URL(REGISTER_URL+s);
                    HttpURLConnection con=(HttpURLConnection)url.openConnection();
                    bufferedReader=new BufferedReader(new InputStreamReader(con.getInputStream()));
                    String result=bufferedReader.readLine();
                    return result;
                }
                catch(Exception e)
                {
                    e.printStackTrace();
                    return null;
                }
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                progressDialog.dismiss();
            }
        }

        Registration ur=new Registration();
        ur.execute(urlsuffix);
    }
}