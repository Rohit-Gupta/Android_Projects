#!/usr/bin/python

""" 
    starter code for exploring the Enron dataset (emails + finances) 
    loads up the dataset (pickled dict of dicts)
    the dataset has the form
    enron_data["LASTNAME FIRSTNAME MIDDLEINITIAL"] = { features_dict }
    {features_dict} is a dictionary of features associated with that person
    you should explore features_dict as part of the mini-project,
    but here's an example to get you started:
    enron_data["SKILLING JEFFREY K"]["bonus"] = 5600000
    
"""

import pickle

enron_data = pickle.load(open("../final_project/final_project_dataset.pkl", "r"))

# How many data points (people) are in the dataset?
len(enron_data)
len(enron_data.keys())

# For each person, how many features are available?
len(enron_data.values()[0])
enron_data[enron_data.keys()[0]]
len(enron_data['SKILLING JEFFREY K'])

# How many POIs are there in the E+F dataset
# Example 1:
count = 0
for i in range(len(enron_data)):
    a = enron_data.values()
    if a[i]['poi'] == True:
        count = count + 1        

# Example 2:
count = 0
for user in enron_data:
    if enron_data[user]['poi'] == True:
        count+=1

# How Many POIs Exist?
poi_text = '../final_project/poi_names.txt'
poi_names = open(poi_text, 'r')
fr = poi_names.readlines()
x=len(fr[2:])
poi_names.close()

# print poi_names.read()

num_lines = sum(1 for line in open(poi_text))

def file_len(fname):
    with open(fname) as f:
        for i, l in enumerate(f):
            pass
    return i + 1

file_len('../final_project/poi_names.txt')

# Check if values exist in any string in the list
matching = [s for s in enron_data.keys() if "PRENTICE" in s]
matching

# What is the total value of the stock belonging to James Prentice?
enron_data['PRENTICE JAMES']['total_stock_value']

# How many email messages do we have from Wesley Colwell to persons of interest?
#[s for s in enron_data.keys() if "COLWELL" in s]
#enron_data['COLWELL WESLEY']
enron_data['COLWELL WESLEY']['from_this_person_to_poi']

print(enron_data['SKILLING JEFFREY K']['exercised_stock_options'])
